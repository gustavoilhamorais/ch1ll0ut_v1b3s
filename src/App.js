import React from "react";
import "./App.css";
import discord from "./discord.png";
import image from "./headerImage.jpg";
function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img className="App-logo" alt="app-logo" src={image} />
        <h1>CH1LL0UT</h1>
        <a
          className="App-link"
          href="https://instagram.com/chillout.vibes?igshid=14tert1lwdl85"
          target="_blank"
          rel="noopener noreferrer"
        >
          <icon className="fa fa-instagram" /> INSTAGRAM
        </a>
        <a
          className="App-link"
          href="https://twitch.tv/chillout_vibes"
          target="_blank"
          rel="noopener noreferrer"
        >
          <icon className="fa fa-twitch" /> TWITCH
        </a>
        <a
          className="App-link"
          href="https://twitter.com/_CH1LL0UT"
          target="_blank"
          rel="noopener noreferrer"
        >
          <icon className="fa fa-twitter" /> TWITTER
        </a>
        <a
          className="App-link"
          href="https://soundcloud.com/ch1ll0utv1b3s"
          target="_blank"
          rel="noopener noreferrer"
        >
          <icon className="fa fa-soundcloud" /> SOUNDCLOUD
        </a>
        <a
          className="App-link"
          href="https://www.youtube.com/channel/UCK4FheldMoUdYNc97v7R7tQ"
          target="_blank"
          rel="noopener noreferrer"
        >
          <icon className="fa fa-youtube" /> YOUTUBE
        </a>
        <a
          className="App-link"
          href="https://discord.gg/nzgaGV"
          target="_blank"
          rel="noopener noreferrer"
        >
          <img className="discord-icon" src={discord} />
          DISCORD
        </a>
      </header>
    </div>
  );
}

export default App;
